#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_ARGS 90
#define MAX_CADENA 100

int extrae_argumentos(char *orig, char *delim, char args[][MAX_CADENA], int max_args)
{
  char *tmp;
  int num=0;
  /* Reservamos memoria para copiar la candena ... pero la memoria justa */
  char *str = malloc(strlen(orig)+1);
  strcpy(str, orig);

  /* Extraemos la primera palabra */
  tmp=strtok(str, delim);
  do
    {
      if (num==max_args)
    return max_args+1;  /* Si hemos extraído más cadenas que palabras devolvemos */
                /* El número de palabras máximo y salimos */

      strcpy(args[num], tmp);   /* Copiamos la palabra actual en el array */
      num++;

      /* Extraemos la siguiente palabra */
      tmp=strtok(NULL, delim);
    } while (tmp!=NULL);

  return num;
}

int main()
{
  char cadena[1000];
  printf("******************************************************************************************\n");
  printf("EScribe la cadena : \n");
    scanf( "%s", cadena);
  printf("******************************************************************************************\n"); 
  printf("******************************************************************************************\n\n");

  char args[MAX_ARGS][MAX_CADENA]; // Para extrae_argumentos
//  char *args2[MAX_ARGS];                  // Para extrae_argumentos_d

char aux [MAX_CADENA];

  int nargs = extrae_argumentos(cadena, " ", args, MAX_ARGS);
  int i,j;

  if (nargs>MAX_ARGS)
    {
    	  printf("******************************************************************************************\n");
      printf ("Se han devuelto m�s palabras del m�ximo\n");
        printf("******************************************************************************************\n");
      nargs=MAX_ARGS;
    }
 printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n");
  printf("CADENA: %s\n", cadena);
    printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n");
  for (i=0; i<nargs; i++){
  	 printf("Palabra %d: %s\n", i, args[i]);
  }
   
    printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n\n");

for (i=0;i<nargs;i++){
for(j=i;j<nargs;j++){
if(strcmp(args[i],args[j])>0){
strcpy(aux, args[i]);
strcpy(args[i], args[j]);
strcpy(args[j], aux);
}
}
}
 printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n");
printf("PALABRAS ORDENADAS\n");
 printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n");
for(i=0; i<nargs; i++){
printf("PALABRA %d:  %s \n",i,args[i]);
}

 printf("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n");
system ("PAUSE");
  return 0;
}
